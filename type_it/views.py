# -*- coding: utf-8 -*-
from django.shortcuts import render
import sys

reload(sys)
sys.setdefaultencoding('utf8')
# Create your views here.

import hashlib
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from models import Text
import json
import requests
from hashlib import md5
import time
import random


@csrf_exempt
def index(request):
    if request.method == 'GET':
        return render(request, 'type_it/index.html')
    if request.method == 'POST':
        last = Text.objects.count()
        if 'i' not in request.session:
            request.session['i'] = random.randint(0, last - 1)
        i = random.randint(0, last - 1)
        while i == request.session['i']:
            i = random.randint(0, last - 1)
        request.session['i'] = i
        text = Text.objects.all()[i].text
        return JsonResponse({'status': 0, 'text': text})
