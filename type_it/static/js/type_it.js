function type_it(text) {
    var counter = 0,
        cursor = "<span>|</span>",
        typedLen = text.length;
    var i = setInterval(function () {
        teletype.innerHTML = text.substr(0, counter) + cursor;
        counter++;
        if (counter == typedLen + 1) {
            clearInterval(i);
        }
    }, 100);
}

function get_text() {
    $.post('/type_it/', function (ret) {
        if (ret['status'] == 0) {
            type_it(ret['text'])
        } else {
            type_it('Loading 。。。')
        }
    });
}

function start() {

    get_text();
    setInterval(function () {
        get_text()
    }, 8000);

}

start();