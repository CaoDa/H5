from django.contrib import admin

# Register your models here.
from type_it.models import Text

admin.site.register(Text)
