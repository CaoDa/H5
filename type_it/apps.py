from __future__ import unicode_literals

from django.apps import AppConfig


class TypeItConfig(AppConfig):
    name = 'type_it'
